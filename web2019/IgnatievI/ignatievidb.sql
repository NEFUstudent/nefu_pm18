CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ignatievidb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `ignatievidb`;

--
-- Table structure for table `auto`
--

DROP TABLE IF EXISTS `auto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Mark` varchar(30) NOT NULL,
  `Year` int(11) NOT NULL,
  `Num` varchar(12) NOT NULL,
  `FIO_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idAUTO_UNIQUE` (`id`),
  KEY `fk_AUTO_FIO1_idx` (`FIO_id`),
  CONSTRAINT `fk_AUTO_FIO1` FOREIGN KEY (`FIO_id`) REFERENCES `fio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auto`
--

LOCK TABLES `auto` WRITE;
/*!40000 ALTER TABLE `auto` DISABLE KEYS */;
INSERT INTO `auto` VALUES (1,'Toyota Chaser',1997,'A228YE014',1);
/*!40000 ALTER TABLE `auto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `city` (
  `id_key` mediumint(9) NOT NULL AUTO_INCREMENT,
  `cityname` char(30) NOT NULL,
  `population` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Ykt',300000);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fio`
--

DROP TABLE IF EXISTS `fio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fio` (
  `id` int(11) NOT NULL,
  `First_name` varchar(15) DEFAULT NULL,
  `Second_name` varchar(15) DEFAULT NULL,
  `Last_name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fio`
--

LOCK TABLES `fio` WRITE;
/*!40000 ALTER TABLE `fio` DISABLE KEYS */;
INSERT INTO `fio` VALUES (1,'Ignatiev','Igor','Evgenevich');
/*!40000 ALTER TABLE `fio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mechanics`
--

DROP TABLE IF EXISTS `mechanics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mechanics` (
  `FIO_Mechanic` varchar(50) NOT NULL,
  `id` int(11) NOT NULL,
  `Staj` varchar(10) DEFAULT NULL,
  `Tabelniy_nomer` int(11) DEFAULT NULL,
  `RAZRYAD_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `FIO_Mechanic_UNIQUE` (`FIO_Mechanic`),
  UNIQUE KEY `id_Mechanic_UNIQUE` (`id`),
  KEY `fk_MECHANICS_RAZRYAD_idx` (`RAZRYAD_id`),
  CONSTRAINT `fk_MECHANICS_RAZRYAD` FOREIGN KEY (`RAZRYAD_id`) REFERENCES `razryad` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mechanics`
--

LOCK TABLES `mechanics` WRITE;
/*!40000 ALTER TABLE `mechanics` DISABLE KEYS */;
INSERT INTO `mechanics` VALUES ('Vasilych',1,'10 let',1,2);
/*!40000 ALTER TABLE `mechanics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `razryad`
--

DROP TABLE IF EXISTS `razryad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `razryad` (
  `id` int(11) NOT NULL,
  `name_RAZRYAD` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `razryad`
--

LOCK TABLES `razryad` WRITE;
/*!40000 ALTER TABLE `razryad` DISABLE KEYS */;
INSERT INTO `razryad` VALUES (1,'Perviy'),(2,'Vtoroy');
/*!40000 ALTER TABLE `razryad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work`
--

DROP TABLE IF EXISTS `work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `Work` varchar(20) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work`
--

LOCK TABLES `work` WRITE;
/*!40000 ALTER TABLE `work` DISABLE KEYS */;
INSERT INTO `work` VALUES (1,'Pokraska',15000);
/*!40000 ALTER TABLE `work` ENABLE KEYS */;
UNLOCK TABLES;
