CREATE TABLE IF NOT EXISTS `Vladelec` (
  `Id_vladelec` INT NOT NULL,
  `Last_name` VARCHAR(45) NULL,
  `First_name` VARCHAR(45) NULL,
  `Middle_name` VARCHAR(45) NULL,
  PRIMARY KEY (`Id_vladelec`)
);


CREATE TABLE IF NOT EXISTS `Cars` (
  `Number` INT NOT NULL,
  `Year` INT NULL,
  `Mark` VARCHAR(45) NULL,
  `Vladelec_Id_vladelec` INT NOT NULL,
  PRIMARY KEY (`Number`),
  FOREIGN KEY (`Vladelec_Id_vladelec`) REFERENCES `Vladelec` (`Id_vladelec`)
);


CREATE TABLE IF NOT EXISTS `Razryad` (
  `Vid` INT NOT NULL,
  PRIMARY KEY (`Vid`)
);

                                                         
CREATE TABLE IF NOT EXISTS `mydb`.`Mehanic` (
  `Tabel_number` INT NOT NULL,
  `Last_name` VARCHAR(45) NULL,
  `First_name` VARCHAR(45) NULL,
  `Middle_name` VARCHAR(45) NULL,
  `Stage` INT NULL,
  `Razryad_Vid` INT NOT NULL,
  PRIMARY KEY (`Tabel_number`),
  FOREIGN KEY (`Razryad_Vid`) REFERENCES `Razryad` (`Vid`)
);


CREATE TABLE IF NOT EXISTS `Rabota` (
  `Id_rabota` VARCHAR(45) NOT NULL,
  `Vid_raboty` INT NULL,
  `Cost` VARCHAR(45) NULL,
  PRIMARY KEY (`Id_rabota`)
);


CREATE TABLE IF NOT EXISTS `Zakazy` (
  `Id_raboty` INT NOT NULL,
  `Date_vydacha` DATETIME NULL,
  `Date_okonchanie_plan` DATETIME NULL,
  `Date_okonchanie_real` DATETIME NULL,
  `Mehanic_Tabel_number` INT NOT NULL,
  `Cars_Number` INT NOT NULL,
  PRIMARY KEY (`Id_raboty`),
  FOREIGN KEY (`Id_raboty`) REFERENCES `Rabota` (`Vid_raboty`),
  FOREIGN KEY (`Mehanic_Tabel_number`) REFERENCES `Mehanic` (`Tabel_number`),
  FOREIGN KEY (`Cars_Number`) REFERENCES `Cars` (`Number`)
);