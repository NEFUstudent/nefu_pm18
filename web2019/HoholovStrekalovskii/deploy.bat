cd ..\..
echo Global setting of variables, 1 time is enough
git config --global user.name NEFUstudent
git config --global user.email study.svfu@gmail.com
echo Adding tracking of new files
git add -A
echo Commiting all local changes, do not forget in comment to show the term of involved changes
git commit -a -m "something new"
echo Uniting all changes from deleted reposition with local changes
git pull origin master
echo Applying commit, merge local changes in deleted reposition
git push origin master
